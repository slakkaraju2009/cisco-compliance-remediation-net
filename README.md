# Cisco Compliance Remediation Devnet Demo
Point your own AAP2 project to this repo to provide the necessary files to run the demo.
This Repo is public so no SCM credentials are required. 

## Install AAP 2.x (Free Trial)
https://www.ansible.com/products/automation-platform

Another option is to sign up for a free developer license.

Only the AAP controller is required. Please have 16gig ram and 4 vcpus for a laptop VM/Rhel OS install.

## controller.yml
This playbook can be modified to configure your Ansible Automation Platform Controller "Tower" for the demo. Afterinstallation you can run this playbook from your laptop to configure your AAP2.x as code.

## Devnet ACI Sandbox
This demo uses the Devnet Reserved Sandbox "Cisco Modeling Labs Enterprise. You are provided free access to this Devnet Sandbox using one of the approved login methods. The reservation can be changed from the default 4 hours to 2 days when scheduled.

 https://devnetsandbox.cisco.com/RM/Diagram/Index/45100600-b413-4471-b28e-b014eb824555?diagramType=Topology

 This demo requires a VPN from your laptop/VM or standalone VM to the Devnet Sandbox. Cisco emails the credentials. 

 Please Note the default topology works for this demo. 

## Run the Devnet-Prepare-Env Job-template
This template maps to the prepare_devnet_env.yml playbook to set the intial confguration for the default CML toplogy "Small NXOS/IOSXE Network".

